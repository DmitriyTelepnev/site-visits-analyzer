<?php
declare(strict_types=1);

use App\Infrastructure\Persistence\Stats\RedisStatsRepository;
use App\Domain\Stats\StatsRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        StatsRepository::class => \DI\autowire(RedisStatsRepository::class),
    ]);
};
