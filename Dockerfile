FROM composer AS builder

WORKDIR /srv/app
COPY . /srv/app
RUN composer install -o --apcu-autoloader --no-dev

FROM php:7.4-fpm-alpine

WORKDIR /var/www
COPY --from=builder /srv/app ./
RUN apk --no-cache add pcre-dev ${PHPIZE_DEPS} \
      && pecl install redis \
      && docker-php-ext-enable redis \
      && apk del pcre-dev ${PHPIZE_DEPS} \
      && rm -rf /tmp/pear \
      && docker-php-ext-install opcache

COPY infra/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini


