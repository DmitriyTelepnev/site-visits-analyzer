# Site visit stat handler

see [conditions](conditions.pdf)

## Run it

```shell
docker-compose up --build --forcre-recreate
```

## Test it

```
dtelepnev@billywest ~ % curl http://localhost/stats/


{
    "statusCode": 200,
    "data": [
        {
            "code": "ru",
            "stats": 9
        },
        {
            "code": "en",
            "stats": 3
        },
        {
            "code": "e",
            "stats": 1
        }
    ]
}
```

```
dtelepnev@billywest ~ % curl -X POST http://localhost/stats/ru

{
    "statusCode": 200
}
```

## Performance

```shell
dtelepnev@billywest site-visits-analyzer % wrk http://localhost/stats/ -t 10 -c 100
Running 10s test @ http://localhost/stats/
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   256.82ms   32.81ms 380.54ms   75.18%
    Req/Sec    38.44     14.47    90.00     70.43%
  3868 requests in 10.09s, 3.34MB read
Requests/sec:    383.34
Transfer/sec:    338.79KB
```