<?php


namespace App\Domain\Stats;


class CountryStat implements \JsonSerializable
{
    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var int
     */
    private $stats;

    public function __construct(string $code, int $stats)
    {
        $this->countryCode = $code;
        $this->stats = $stats;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return self
     */
    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStats(): int
    {
        return $this->stats;
    }

    /**
     * @param int $stats
     * @return self
     */
    public function setStats(int $stats): self
    {
        $this->stats = $stats;
        return $this;
    }

    /**
     * @inheritDoc
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'code' => $this->countryCode,
            'stats' => $this->stats
        ];
    }

}