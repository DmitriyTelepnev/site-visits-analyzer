<?php


namespace App\Domain\Stats;


interface StatsRepository
{

    /**
     * @return CountryStat[]
     */
    public function getAll(): array;

    /**
     * @param string $countryCode
     */
    public function incrementByCode(string $countryCode);

}