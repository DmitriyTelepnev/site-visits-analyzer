<?php


namespace App\Domain\Stats;


use App\Domain\DomainException\DomainException;
use Throwable;

class InvalidCountryCodeException extends DomainException
{

    const BAD_REQUEST = 400;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, self::BAD_REQUEST, $previous);
    }

}