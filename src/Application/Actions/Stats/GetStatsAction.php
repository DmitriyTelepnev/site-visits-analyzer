<?php


namespace App\Application\Actions\Stats;


use Psr\Http\Message\ResponseInterface as Response;

class GetStatsAction extends StatsAction
{

    /**
     * Get all country stats
     * @return Response
     */
    protected function action(): Response
    {
        return $this->respondWithData($this->statsRepository->getAll());
    }
}