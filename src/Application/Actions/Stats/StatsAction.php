<?php


namespace App\Application\Actions\Stats;


use App\Application\Actions\Action;
use App\Domain\Stats\StatsRepository;
use Psr\Log\LoggerInterface;

abstract class StatsAction extends Action
{

    protected $statsRepository;

    public function __construct(LoggerInterface $logger, StatsRepository $repository)
    {
        parent::__construct($logger);
        $this->statsRepository = $repository;
    }

}