<?php


namespace App\Application\Actions\Stats;


use App\Adapter\Validator\CountryCodeValidator;
use App\Domain\Stats\InvalidCountryCodeException;
use Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ResponseInterface as Response;

class AddStatsAction extends StatsAction
{

    /**
     * Increment stats by countryCode
     * @return Response
     * @throws HttpBadRequestException
     * @throws InvalidCountryCodeException
     */
    protected function action(): Response
    {
        $countryCode = $this->resolveArg('countryCode');
        if(!CountryCodeValidator::isValidCountryCode($countryCode)) {
            throw new InvalidCountryCodeException();
        }

        $this->statsRepository->incrementByCode(mb_strtolower($countryCode));
        return $this->respondWithData();
    }
}