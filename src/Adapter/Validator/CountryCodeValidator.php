<?php


namespace App\Adapter\Validator;


class CountryCodeValidator
{

    const COUNTRY_CODE_LENGTH = 2;

    /**
     * @param string $countryCode
     * @return bool
     */
    public static function isValidCountryCode(string $countryCode): bool
    {
        return mb_strlen($countryCode) === self::COUNTRY_CODE_LENGTH
            && !self::isNumbersExists($countryCode);
    }

    public static function isNumbersExists(string $countryCode): bool {
        return preg_match('/[0-9]/', $countryCode) !== 0;
    }

}