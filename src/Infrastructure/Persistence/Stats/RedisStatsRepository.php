<?php


namespace App\Infrastructure\Persistence\Stats;


use App\Domain\Stats\CountryStat;
use App\Domain\Stats\StatsRepository;

class RedisStatsRepository implements StatsRepository
{

    const MAP_NAME = 'country_stats';

    /**
     * @var \Redis
     */
    private $redisConn;

    public function __construct(\Redis $redis)
    {
        $this->redisConn = $redis;
    }

    /**
     * @return CountryStat[]
     */
    public function getAll(): array
    {
        $data = $this->redisConn->hGetAll(self::MAP_NAME);
        $allStats = [];
        foreach ($data as $country => $stats) {
            $allStats[] = new CountryStat($country, $stats);
        }

        return $allStats;
    }


    /**
     * @param string $countryCode
     */
    public function incrementByCode(string $countryCode)
    {
        $this->redisConn->hIncrBy(self::MAP_NAME, $countryCode, 1);
    }


}